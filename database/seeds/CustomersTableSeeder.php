<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = factory(\App\Customer::class, 20)->create();

        $cars      = factory(\App\Car::class, 20)->create();
        $customers->each(function ($customer) use ($cars) {
            $customer->cars()->attach(
                $cars->random(rand(5, 10))->pluck('id')->toArray()
            );
        });
    }
}
