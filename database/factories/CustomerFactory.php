<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Car;
use App\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'name'    => $faker->name,
        'mobile'  => $faker->phoneNumber,
        'address' => $faker->address
    ];
});

$factory->define(Car::class, function (Faker $faker) {
    $faker = (new \Faker\Factory())::create();
    $faker->addProvider(new \Faker\Provider\Fakecar($faker));
    return [
        'name'    => $faker->vehicle,
    ];
});
