<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class CheckHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = apache_request_headers();

        return $header['x-api-key'] === 'test' ? $next($request) : \response(['message'=> 'error'], Response::HTTP_UNAUTHORIZED);
    }

}
