<?php

namespace App\Http\Controllers\Api\v1;

use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\v1\CustomerRequest;
use App\Http\Resources\v1\CarResourceCollection;
use App\Http\Resources\v1\CustomerResource;


class CustomerController extends Controller
{
    public function getAction()
    {
        $customers = Customer::with('cars')->get();

        return CustomerResource::collection($customers);
    }

    public function createAction(CustomerRequest $request)
    {
        $data = $request->all();
        $customer = new Customer();
        $customer->fill($data);
        $customer->save();

        return response([
            'data'   => $customer,
            'status' => 'success'
        ], 200);
    }

    public function updateAction(CustomerRequest $request, Customer $customer)
    {
        $data = $request->all();
        $customer->fill($data);
        $customer->save();

        return response([
            'data'   => $customer,
            'status' => 'success'
        ], 200);

    }

    public function deleteAction(Customer $customer)
    {
        $customer->delete();

        return response([
            'data'   => '',
            'status' => 'success'
        ], 200);
    }
}
