<?php namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;


trait FilterMe
{
    public function scopeFilterColumn(Builder $query, Collection $filters, string $item, string $operator = '=')
    {
        $filled = $filters->get($item);
        if ($filled !== '' && $filled !== null) {
            $query->where($item,
            $operator,
            $filters->get($item)
            );
        }

        return $query;
    }

    public function scopeFilterStrColumn(Builder $query, Collection $filters, string $item)
    {
        $filled = $filters->get($item);
        if ($filled !=='' && $filled !== null)
        {
            $query->where($item, 'like', $filters->get($item).'%');
        }

        return $query;
    }





}
