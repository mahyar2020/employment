<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'name',
        'mobile',
        'address'
    ];

    public function cars()
    {
        return $this->belongsToMany(Car::class);
    }
}
