$('.home-slider').owlCarousel({
    loop:true,
    rtl:true,
    items:1,
    autoplay:true,

});
$('.brand-slider').owlCarousel({
    loop:true,
    rtl:true,
    items:5,
    autoplay:true,
    margin:10,
    responsive: {
        0:{
            items:2,
        },
        570:{
            items:3,
        },
        768:{
            items:4,
        },
        922:{
            items:5,
        }
    }
});
/*
$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: 'http://zarin-service.com/blog/wp-json/wp/v2/posts?per_page=3',

        success: function (data) {
            console.log(data)
            var posts_html = '';
            $.each(data, function (index, post) {
                posts_html += '<a href="' + post.link + '"><h2>' + post.title.rendered + '</h2></a>';
                posts_html += '<p>' + post.excerpt.rendered + '</p>';
            });
            $('#posts').html(posts_html);
        },
        error: function (request, status, error) {
            alert(error);
        }
    });
});
*/

