@extends('layouts.master')
@section('title')
    List Customers
@endsection
@section('head-page-link')
    <link href="{{ asset('admin-theme/assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet">
@endsection


@section('content')
    <div class="box box-primary">

        <div class="box-body">
            <div class="col-sm-12">
                <div class="white-box">

                    <div class="table-responsive"  style="direction: ltr">
                        <table id="myTable" class="table table-striped display nowrap">
                            <thead>
                            <tr>
                                <th>name</th>
                                <th>mobile</th>
                                <th>address</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{$customer->name}}</td>
                                    <td>{{$customer->mobile}}</td>
                                    <td>{{$customer->address}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer-page-link')
    <script src="{{ asset('admin-theme/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>

    <script>
        $(document).ready(function () {

            $('#myTable').DataTable({

            });
        });
    </script>
@endsection
