<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('admin-theme/assets/plugins/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/assets/plugins/bootstrap-extension/css/bootstrap-extension.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/assets/plugins/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/assets/css/colors/default.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/assets/plugins/animate/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/assets/plugins/material-design-iconic-font/css/material-design-iconic-font.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/assets/plugins/weather-icons/css/weather-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/assets/plugins/themify-icons/css/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/assets/plugins/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('admin-theme/assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">

    @yield('head-page-link')
</head>
<body dir="ltr">
<!-- Preloader -->
<div class="preloader">
    <svg class="circular" viewbox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
    </svg>
</div>
<div>

    <div >
        <div class="container">
            <div class="row bg-title" style="text-align: left">
                <div class="col-12">
                    <h4 class="page-title">@yield('title')</h4>
                </div>
            </div>
            @yield('content')

        </div>

    </div>


</div>

<script src="{{ asset('admin-theme/assets/plugins/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('admin-theme/assets/plugins/bootstrap/dist/js/tether.min.js') }}"></script>
<script src="{{ asset('admin-theme/assets/plugins/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-theme/assets/plugins/bootstrap-extension/js/bootstrap-extension.min.js') }}"></script>
<script src="{{ asset('admin-theme/assets/plugins/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
<script src="{{ asset('admin-theme/assets/plugins/jquery.slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('admin-theme/assets/plugins/waves/waves.min.js') }}"></script>
<script src="{{ asset('admin-theme/assets/js/custom.js') }}"></script>
<script src="{{ asset('admin-theme/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.min.js') }}"></script>
<script src="{{ asset('admin-theme/assets/js/style-switcher.js') }}"></script>
<script src="{{ asset('admin-theme/assets/plugins/switchery/dist/switchery.min.js') }}"></script>
<script src="{{ asset('admin-theme/assets/plugins/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin-theme/assets/plugins/multiselect/js/jquery.multi-select.js') }}" ></script>
<script src="{{ asset('admin-theme/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>

@yield('footer-page-link')
</body>
</html>
